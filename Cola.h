#ifndef ECOLA
#define ECOLA
#include <iostream>
#include "DobNode.h"
#include "beep.cpp"
using namespace std;
template <class E>
class Cola{
	public:
		DobNode<E> *head;
		DobNode<E> *tail;
		DobNode<E> *aux;
		int tamano;
		Cola(){
			head = tail = aux = NULL;
			tamano = 0;
		}
		~Cola() { clear(); }
		void clear(){
			while (head){
				desencolar();
			}
			tamano = 0;
			head = tail = NULL;
		}
		void goToPos(int nPos){
			aux = head;
			for(int i=0;i<nPos;i++){
				aux = aux->prev;
			}
		}
		void cambiaN(int pos, E elem, float time){
			goToPos(pos);
			aux->element = elem;
			aux->tiempo = time;
		}
		void encolar(E elem,float time){
			if (!head && !tail){
				head = tail = new DobNode<E>(elem,NULL,NULL,time);
			} else {
				DobNode<E> *temp = new DobNode<E>(elem,tail,NULL,time);
				tail->prev = temp;
				tail = temp;
			}
		}
		float getTime(Node<E> *nodo){
			float temp = nodo->tiempo;
			return temp;
		}
		E desencolar(){
			DobNode<E> *temp = head;
			E elem = temp->element;
			head = head->prev;
			delete temp;
			return elem;
		}
		E frente(){
			return head->element;
		}
		bool vacia(){
			return (head==NULL);
		}
		void imprime(){
			DobNode<E> *temp = head;
			cout << "Elementos de la Cola\n";
			while (head){
				cout << head->element << " ";
				head = head->prev;
			}
			head = temp;
			cout << endl;
		}
		void makeBeep(){
			if (head==NULL){
				cout << "La cola fue vaciada\n";
				return;
			}
			float t;
			while (head){
				t = head->tiempo;
				E nota = desencolar();
				beeper(int(nota),t);
			}
			cout << "La cola fue vaciada\n";
		}
};
#endif
