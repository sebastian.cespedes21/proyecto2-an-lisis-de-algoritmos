#ifndef LlDOBLINKEDLIST
#define LlDOBLINKEDLIST
#include <iostream>
#include "DobNode.h"
using namespace std;
template <class Ll>

class DobLinkedList{
	public:
		DobNode<Ll> *head;
		DobNode<Ll> *curr;
		DobNode<Ll> *tail;
		int tamano;
		DobLinkedList(){
			head = tail = curr = NULL;
			tamano = 0;
		}
		Ll getcurr(){
			return curr->element;
		}
		void goToStart(){
			curr = head;
		}
		void goToEnd(){
			curr = tail;
		}
		void goToPos(int nPos){
			curr = head;
			for(int i=0;i<nPos;i++){
				curr = curr->next;
			}
		}
		void cambiaN(int pos, Ll elem, float time){
			goToPos(pos);
			curr->element = elem;
			curr->tiempo = time;
		}
		int getPos(){
			DobNode<Ll> *temp = head;
			int pos = 0;
			while (temp != curr){
				pos++;
				temp = temp->next;
			}
			return pos;
		}
		int getSize(){
			return tamano;
		}
		void previous(){
			curr = curr->prev;
		}
		void next(){
			curr = curr->next;
		}
		void appendN(DobNode<Ll>* pelement){
			if (!head && !tail && !curr){
				head = tail = curr = pelement;
				tamano++;
			} else {
				tail->next = pelement;
				tamano++;
				pelement->prev = tail;
				tail = pelement;
			}
		}
		void append(Ll pelement,float time){
			if (!head && !tail && !curr){
				head = tail = curr = new DobNode<Ll>(pelement,NULL,NULL,time);
				tamano++;
			} else {
				DobNode<Ll> *temp = new DobNode<Ll>(pelement,NULL, tail,time);
				tamano++;
				tail->next = temp;
				tail = temp;
			}
		}
		void insert(Ll pelement){
			if (!head && !tail && !curr){
				head = tail = curr = new DobNode<Ll>(pelement);
				tamano++;
			} else {
				DobNode<Ll> *temp = new DobNode<Ll>(pelement, curr, curr->prev);
				curr->prev->next = temp;
				curr->prev = temp;
				tamano++;
			}
		}
		void insertHead(Ll pelement){
			if (!head && !tail && !curr){
				head = tail = curr = new DobNode<Ll>(pelement);
				tamano++;
			} else {
				DobNode<Ll> *temp = new DobNode<Ll>(pelement, head, NULL);
				head->prev = temp;
				head = temp;
				tamano++;
			}
		}
		void remove(){
			DobNode<Ll> *temp = curr;
			curr->prev->next = curr->next;
			curr->next->prev = curr->prev;
			curr = temp->prev;
			delete temp;
		}
		bool vacia(){
			return head==NULL;
		}
		void circular(){
			tail->next = head;
			head->prev = tail;
		}
		void imprime(){
			DobNode<Ll> *temp = head;
			cout << "Elementos de la DLL\n";
			while (head){
				cout << head->element << " ";
				head = head->next;
			}
			cout << endl;
			head = temp;
		}
		void makeBeep(){
			DobNode<Ll> *temp = head;
			while (head){
				beeper(int(head->element),head->tiempo);
				head = head->next;
			}
			head = temp;
			temp = tail;
			while (tail){
				beeper(int(tail->element),tail->tiempo);
				tail = tail->prev;
			}
			tail = temp;
		}
		void clear(){
			goToStart();
			DobNode<Ll> *temp = head;
			while (temp!=NULL){
				delete head;
				head = temp = temp->next;
			}
			head = tail = curr = NULL;
		}
		~DobLinkedList(){
			delete head;
			delete tail;
		}
};

#endif
