#ifndef TnDOBNODE
#define TnDOBNODE
#include <iostream>

template <class Tn>
class DobNode{
	public:
		Tn element;
		float tiempo = 0.1;
		DobNode<Tn>* next;
		DobNode<Tn>* prev;

		DobNode(Tn pElement, DobNode<Tn>* pNext = NULL, DobNode<Tn>* pPrev = NULL, float ptiempo = 0.001){
			element = pElement;
			next = pNext;
			prev = pPrev;
			tiempo = ptiempo;
		}
		DobNode(DobNode<Tn>* pNext = NULL, DobNode<Tn>* pPrev = NULL){
			next = pNext;
			prev = pPrev;
		}
};
#endif 
