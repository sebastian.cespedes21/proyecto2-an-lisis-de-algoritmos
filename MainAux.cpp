#include <unistd.h>
#include <iostream>
#include "beep.cpp"
#include "SimpLinkedList.h"
#include "DobLinkedList.h"
#include "Cola.h"
#include "Pila.h"
using namespace std;

int main (){
	char letra;
	float tiempo;
	cout << "Digite la nota y la duración de la misma\n";
	cin >> letra >> tiempo;
	int numletra = int(letra);
	SimpLinkedList<char> SLL;
	DobLinkedList<char> DLL;
	Cola<char> Cola1;
	Pila<char> Pila1;
	while (numletra!=125){
		SLL.append(letra,tiempo);
		DLL.append(letra,tiempo);
		Cola1.encolar(letra,tiempo);
		Pila1.push(letra,tiempo);
		beeper(numletra, tiempo);
		system("clear");
		Pila1.imprime();
		Cola1.imprime();
		SLL.imprime();
		DLL.imprime();
		cout << "---------------------------";
		cout << "\nDigite la siguiente nota\nPara terminar de introducir notas digite:	}0\n";
		cin >> letra >> tiempo;
		numletra = int(letra);
		system("clear");
	}
	Pila1.imprime();
	Cola1.imprime();
	SLL.imprime();
	DLL.imprime();
	cout << "---------------------------";
	cout << "\nDesea cambiar algún elemento introducido?\n1 = Sí\n0 = No\n";
	int pos = 0;
	cin >> pos;
	system("clear");
	Pila1.imprime();
	Cola1.imprime();
	SLL.imprime();
	DLL.imprime();
	cout << "---------------------------\n";
	while (pos){
		cout << "Cual elemento desea cambiar?\nDigite la posición en la que se encuentre\n";
		cin >> pos;
		system("clear");
		Cola1.imprime();
		SLL.imprime();
		DLL.imprime();
		cout << "---------------------------\n";
		while (pos>DLL.getSize()){
			system("clear");
			Cola1.imprime();
			SLL.imprime();
			DLL.imprime();
			cout << "---------------------------";
			cout << "\nEsa posición está fuera del rango de elementos\nDigite una posición válida\n";
			cin >> pos;
		}
		system("clear");
		Cola1.imprime();
		SLL.imprime();
		DLL.imprime();
		cout << "---------------------------\n";
		cout << "Digite la nueva nota\n";
		cin >> letra >> tiempo;
		SLL.cambiaN(pos, letra, tiempo);
		DLL.cambiaN(pos, letra, tiempo);
		Cola1.cambiaN(pos, letra, tiempo);	
		system("clear");
		Cola1.imprime();
		SLL.imprime();
		DLL.imprime();
		cout << "---------------------------\n";
		cout << "Desea cambiar alguna otra nota?\n1 = Sí\n0 = No\n";
		cin >> pos;
	}
	Pila1.clear();
	DLL.goToStart();
	while (DLL.curr){
		Pila1.push(DLL.getcurr(),DLL.curr->tiempo);
		DLL.next();
	}
	system("clear");
	Pila1.imprime();
	Cola1.imprime();
	SLL.imprime();
	DLL.imprime();
	cout << "---------------------------\n";
	cout << "Cual estructura desea reproducir?\n";
	cout << "1 = Cola\n2 = SLL\n3 = DLL\n4 = Invertir las notas de las estructuras\n5 = Salir\n";
	int Reproductor;
	cin >> Reproductor;
	while (Reproductor!=5){	
		switch (Reproductor){
			case (1):
				Cola1.makeBeep();
				break;
			case (3):
				DLL.makeBeep();
				break;
			case (2):
				SLL.makeBeep();
				break;
			case (4):
				if (Pila1.vacia()){
					cout << "La pila está vacía\n";
					break;
				}
				cout << "Invirtiendo las estructuras\n\n\n";
				SLL.clear();
				DLL.clear();
				Cola1.clear();
				while (!Pila1.vacia()){
					float t = Pila1.getTime(Pila1.top);
					char e = Pila1.pop();
					SLL.append(e,t);
					DLL.append(e,t);
					Cola1.encolar(e,t);
				}
				usleep(750000);
				cout << "Se vació la pila\n";
				break;
			default:
				break;
		}
		usleep(1000000);
		system("clear");
		Pila1.imprime();
		Cola1.imprime();
		SLL.imprime();
		DLL.imprime();
		cout << "Cual estructura desea reproducir?\n";
		cout << "1 = Cola\n2 = SLL\n3 = DLL\n4 = Invertir las notas de las estructuras\n5 = Salir\n";
		cin >> Reproductor;
	}
	return 0;
}
int crono(int F){
	int segundos = 0;
	for(;;){
		segundos++;
		cout << segundos << endl;
		if (segundos == 10) {segundos = 0;}
		usleep(1000000);
	}
}
