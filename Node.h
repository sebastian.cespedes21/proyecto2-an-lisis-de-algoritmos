#ifndef TnNODE
#define TnNODE
#include <iostream>

template <class Tn>
class Node{
	public:
		Tn element;
		float tiempo = 0.001;
		Node<Tn>* next;
		
		Node(Tn pElement, Node<Tn>* pNext = NULL, float ptiempo = 0.001){
			element = pElement;
			next = pNext;
			tiempo = ptiempo;
		}
		Node(Node<Tn>* pNext = NULL){
			next = pNext;
		}
};
#endif 
