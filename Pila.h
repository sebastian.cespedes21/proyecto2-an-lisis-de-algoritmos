#ifndef EPILA
#define EPILA
#include "Node.h"
using namespace std;
template <class E>
class Pila{
public:
	Node<E> *top = NULL;
	Node<E> *aux = NULL;
	int tamano = 0;
	Pila(){
		top = NULL; 
		tamano = 0;
	}
	~Pila() { clear(); };
	void clear(){
		while (top!=NULL){
			Node<E>* temp = top;
			top = top->next;
			delete temp;
		}
		tamano = 0;
	}/*
	void goToPos(int nPos){
		aux = top;
		int temp = tamano;
		while (nPos!=temp){
			cout << "while\n";
			aux = aux->next;
			cout << "next\n";
			temp--;
			nPos++;
		}
	}
	void cambiaN(int pos, E elem, float time){
		goToPos(pos);
		aux->element = elem;
		aux->tiempo = time;
	}*/
	void push(E elem,float time){
		Node<E> *temp = new Node<E>(elem, top, time);
		top = temp;
		tamano++;
	}
	float getTime(Node<E> *nodo){
		float temp = nodo->tiempo;
		return temp;
	}
	E pop(){
		E elem = top->element;
		Node<E> *temp = top;
		top = top->next;
		delete temp;
		tamano--;
		return elem;
	}
	E actual(){
		return top->element;
	}
	bool vacia(){
		return top==NULL;
	}
	void imprime(){
		Node<E> *temp = top;
		cout << "Elementos de la pila\n";
		while (top){
			cout << top->element << " ";
			top = top->next;
		}
		top = temp;
		cout << endl;
	}
};
#endif
