#ifndef SLlSIMPLINKEDLIST
#define SLlSIMPLINKEDLIST
#include <iostream>
#include "Node.h"
using namespace std;
template <class SLl>

class SimpLinkedList{
	public:
		Node<SLl> *head;
		Node<SLl> *curr;
		Node<SLl> *tail;
		int tamano;
		SimpLinkedList(){
			head = tail = curr = NULL;
			tamano = 0;
		}
		SLl getcurr(){
			return curr->element;
		}
		void goToStart(){
			curr = head;
		}
		void goToEnd(){
			curr = tail;
		}
		void goToPos(int nPos){
			curr = head;
			for(int i=0;i<nPos;i++){
				curr = curr->next;
			}
		}
		void cambiaN(int pos, SLl elem, float time){
			goToPos(pos);
			curr->element = elem;
			curr->tiempo = time;
		}
		int getPos(){
			Node<SLl> *temp = head;
			int pos = 0;
			while (temp != curr){
				pos++;
				temp = temp->next;
			}
			return pos;
		}
		int getSize(){
			return tamano;
		}
		void previous(){
			if (curr == head){return;}
			Node<SLl> *temp = curr;
			curr = head;
			while (curr->next!=temp){
				curr = curr->next;
			}
			return;
		}
		void next(){
			if (curr==tail){return;}
			curr = curr->next;
		}
		void appendN(Node<SLl>* pelement){
			if (!head && !tail && !curr){
				head = tail = curr = pelement;
				tamano++;
			} else {
				tail->next = pelement;
				tamano++;
				tail = pelement;
			}
		}
		void append(SLl pelement,float tiempo){
			if (!head && !tail && !curr){
				head = tail = curr = new Node<SLl>(pelement,NULL, tiempo);
				tamano++;
			} else {
				Node<SLl> *temp = new Node<SLl>(pelement,NULL, tiempo);
				tamano++;
				tail->next = temp;
				tail = temp;
			}
		}
		void insert(SLl pelement){
			if (!head && !tail && !curr){
				head = tail = curr = new Node<SLl>(pelement);
				tamano++;
			} else {
				Node<SLl> *temp = new Node<SLl>(pelement, curr);
				if (curr==head){
					tamano++;
				} else {
					previous();
					curr->next = temp;
					tamano++;
				}
			}
		}
		void insertHead(SLl pelement){
			if (!head && !tail && !curr){
				head = tail = curr = new Node<SLl>(pelement);
				tamano++;
			} else {
				Node<SLl> *temp = new Node<SLl>(pelement, head);
				head = temp;
				tamano++;
			}
		}
		void remove(){
			if (curr==head){
				Node<SLl> *temp = curr->next;
				~curr;
				curr = head = temp;
				return;
			}
			Node<SLl> *temp = curr;
			previous();
			curr->next = temp->next;
			delete temp;
		} 
		void circular(){
			tail->next = head;
		}
		bool vacia(){
			return head==NULL;
		}
		void clear(){
			goToStart();
			Node<SLl> *temp = head;
			while (temp!=NULL){
				delete head;
				head = temp = temp->next;
			}
			head = tail = curr = NULL;
		}
		void imprime(){
			Node<SLl> *temp = head;
			cout << "Elementos de la SLL\n";
			while (head){
				cout << head->element << " ";
				head = head->next;
			}
			cout << endl;
			head = temp;
		}
		void makeBeep(){
			Node<SLl> *temp = head;
			float t;
			while (head){
				t = head->tiempo;
				beeper(int(head->element), t);
				head = head->next;
			}
			head = temp;
		}
		~SimpLinkedList(){ clear(); }
};

#endif
