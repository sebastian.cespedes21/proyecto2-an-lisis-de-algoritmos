#ifndef BEEP_CPP
#define BEEP_CPP
#include <stdio.h>
#include <iostream>

// g++ -o beep sound.cpp -lasound -lm

#include <ctype.h>
#include <alsa/asoundlib.h>
#include <alsa/pcm.h>
#include <math.h>
#define BUFFER_LEN 48000

static char device [] = "default";                       //soundcard
snd_output_t *output = NULL;
float buffer [BUFFER_LEN];

void beep(int frequency, float miliseconds)
{
    int err;
    int j,k;

    int f = frequency;                //frequency
    int fs = 38000;             //sampling frequency
    int buffer_size = BUFFER_LEN;
    int seconds = (int) (miliseconds/1000);
    float time_fraction = (miliseconds/1000) - seconds;

    snd_pcm_t *handle;
    snd_pcm_sframes_t frames;


    // ERROR HANDLING

    if ((err = snd_pcm_open(&handle, device, SND_PCM_STREAM_PLAYBACK, 0)) < 0) {
            printf("Playback open error: %s\n", snd_strerror(err));
            exit(EXIT_FAILURE);
    }

    if ((err = snd_pcm_set_params(handle,
                                  SND_PCM_FORMAT_FLOAT,
                                  SND_PCM_ACCESS_RW_INTERLEAVED,
                                  1,
                                  38000,
                                  1,
                                  100000)) < 0) {   
            printf("Playback open error: %s\n", snd_strerror(err));
            exit(EXIT_FAILURE);


    }

        for (k=0; k<BUFFER_LEN; k++){

            buffer[k] = (sin(2*M_PI*f/fs*k));                 //sine wave value generation                        
        }       

        for (j=0; j< seconds; j++){
            frames = snd_pcm_writei(handle, buffer, BUFFER_LEN);    //sending values to sound driver
        }

        if (time_fraction > 0)
            frames = snd_pcm_writei(handle, buffer, BUFFER_LEN * time_fraction );    //sending values to sound driver   

    snd_pcm_close(handle);

    return;

}

void print_usage() {
    printf("Usage: -f requires frequency, -t requires time in miliseconds\n\n");
    printf("Example: ./beep -f 440 -t 1000\n\n");
}

void beeper(int nota,float tiempo){
	tiempo = tiempo*1000;
	if (nota==35){
		beep(0,tiempo);
	}
	if (nota==97){
		beep(261.626,tiempo);
	}
	if (nota==119){
		beep(277.183,tiempo);
	}
	if (nota==115){
		beep(293.665,tiempo);
	}
	if (nota==101){
		beep(311.127,tiempo);
	}
	if (nota==100){
		beep(329.628,tiempo);
	}
	if (nota==102){
		beep(349.228,tiempo);
	}
	if (nota==116){
		beep(369.994,tiempo);
	}
	if (nota==103){
		beep(391.995,tiempo);
	}
	if (nota==121){
		beep(415.305,tiempo);
	}
	if (nota==104){
		beep(440,tiempo);
	}
	if (nota==117){
		beep(452.164,tiempo);
	}
	if (nota==106){
		beep(493.88,tiempo);
	}
	if (nota==65){
		beep(523.251,tiempo);
	}
	if (nota==87){
		beep(554.265,tiempo);
	}
	if (nota==83){
		beep(587.33,tiempo);
	}
	if (nota==69){
		beep(622.254,tiempo);
	}
	if (nota==68){
		beep(659.255,tiempo);
		}
	if (nota==70){
		beep(698.456,tiempo);
	}
	if (nota==84){
		beep(739.989,tiempo);
	}
	if (nota==71){
		beep(783.991,tiempo);
	}
	if (nota==89){
		beep(830.609,tiempo);
	}
	if (nota==72){
		beep(880,tiempo);
	}
	if (nota==85){
		beep(932.328,tiempo);
	}
	if (nota==74){
		beep(987.767,tiempo);
	}
}
#endif
