#include <iostream>
#include <fstream>
using namespace std;

int main(int argc, char* argv[])
{
	//open arduino device file (linux)
        std::ofstream arduino;
	arduino.open( "/dev/ttyACM0");

	//write to it
        arduino << "Hello from C++!";
	arduino.close();

	return 0;
}
/*
int main()
{
   Fichero original, se abre para lectura pasando parámetros en la declaración de la variable
   ifstream f("musica.txt", ifstream::in);

   Fichero nuevo para copiar, se abre después de declararlo, llamando a open()
   ofstream f2;
   f2.open("musica2.txt", ofstream::out);
   char cadena[100];

Leemos
f >> cadena;
cout << cadena << endl;
y escribimos
f2 << cadena;


f.close();
f2.close();
}
*/
